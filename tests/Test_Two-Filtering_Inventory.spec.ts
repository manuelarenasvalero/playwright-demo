import { test, expect } from '@playwright/test';

test('test 2', async ({ page }) => {

  await page.goto('https://builder.knack.com/');

// SECTION - LOGIN

  await page.getByLabel('Email Address').fill('mav3@gmail.com');
  await page.getByLabel('Password').fill('Knack2023');
  await page.getByRole('button', { name: 'Sign In' }).click();
  await expect(page).toHaveURL('https://builder.knack.com/');

/* REVIEW - LOGIN WORKAROUND
  await page.fill('#email >> nth=1', accountEmail);
  await page.fill('#password >> nth=1', accountPass);
  await page.click('input[type="submit"] >> nth=1');
*/
// !SECTION

// SECTION - SETUP
  await page.getByRole('link', { name: 'Warehouse Manager' }).click();
  await expect(page).toHaveURL('https://builder.knack.com/mav3/warehouse-manager');

  await page.getByRole('link', { name: 'Records' }).click();
  await expect(page).toHaveURL('https://builder.knack.com/mav3/warehouse-manager/records');

  await page.locator('a:has-text("Warehouse Inventory")').click();
  await expect(page).toHaveURL('https://builder.knack.com/mav3/warehouse-manager/records/objects/object_20');

  await page.locator('a:has-text("Add filters")').click();
  await page.locator('select').first().selectOption('field_142');
  await page.locator('select').nth(1).selectOption('is');
  await page.locator('select').nth(2).selectOption('true');
  await page.getByRole('button', { name: 'Submit' }).click();

  await expect(page.getByRole('cell', { name: 'Needs Re-Order' })).toBeVisible;


  // NOTE - Extracting number of rows on builder

  let num_rows = 0;
  let i = 0;

  await page.waitForLoadState('networkidle'); 
  
  for (i = 0 ; i < await page.getByRole('table').locator('tbody > tr').count() ; i++)
    if (await page.locator('#tableCell-'+i+'-10').isVisible())
      num_rows++;

  for (i = 0 ; i < num_rows ; i++)
    await expect(page.locator('#tableCell-'+i+'-10')).toHaveText('Yes');

// !SECTION - SETUP

// SECTION - TEST

  // SECTION - LOGIN INTO APP

  const [page1] = await Promise.all([
    page.waitForEvent('popup'),
    page.getByRole('link', { name: 'Go to Live App' }).click()
  ]);
  
  await page1.getByLabel('Email Address').fill('admin@test.com');
  await page1.getByLabel('Password\n          (forgot?)').fill('test');
  await page1.getByRole('button', { name: 'Sign In' }).click();

  // !SECTION - LOGIN INTO APP

  // SECTION - REACHING DESTINATION

  await page1.locator('nav:has-text("Home Products Inventory Suppliers Customers Customer Orders Warehouses Reports S")').getByRole('link', { name: ' Inventory' }).click();
  await expect(page1).toHaveURL('https://mav3.knack.com/warehouse-manager#inventory2/');

  await page1.locator('a:has-text("Add filters")').click();
  await page1.locator('span:has-text("ProductWarehouseLocation in WarehouseSell PriceTotal ReceivedTotal ShippedOn-Han") select[name="field"]').selectOption('field_142');
  await page1.locator('span:has-text("isis notis blankis not blank") select[name="operator"]').selectOption('is');
  await page1.locator('select[name="value"]').selectOption('Yes');
  await page1.getByRole('button', { name: 'Submit' }).click();

  await expect(page1.getByRole('cell', { name: 'Re-Order? ' })).toBeVisible;

  await page1.waitForLoadState('networkidle');

  // !SECTION - REACHING DESTINATION

  // SECTION - Validate that EVERY “Needs Re-Order” table cell is set to “Yes”.

  // NOTE - Counting visible rows
  let visible_rows = 0;
  for (i = 0 ; i < await page.getByRole('table').locator('tbody > tr').count() ; i++)
    if (page1.getByRole('table').locator('tbody > tr .field_142 > .col-7:visible').nth(i))
      visible_rows++;

  console.log("total rows on table = " + await page1.getByRole('table').locator('tbody > tr').count());
  console.log("visible data rows = " + visible_rows);

  // NOTE: Counting visible rows with text cell set to 'Yes' on 'Re-Order?' column
  let num_need_reorder = await page1.getByRole('table').locator('tbody > tr .field_142 > .col-7:visible', { hasText: 'Yes' }).count();

  console.log("Visible rows that need re-order = " + num_need_reorder);

  // !SECTION - Validate that EVERY “Needs Re-Order” table cell is set to “Yes”.

  // SECTION - Validate that the number of records matches the number of records shown in the builder Records Tab (the number from step 6).

  expect(num_rows).toEqual(num_need_reorder);

  // !SECTION - Validate that the number of records matches the number of records shown in the builder Records Tab (the number from step 6).

// !SECTION - TEST

});
