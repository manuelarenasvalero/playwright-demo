import { test, expect } from '@playwright/test';
import { color } from 'pengrape';

test.only('test 1', async ({ page }) => {

  await page.goto('https://builder.knack.com/');

// SECTION - LOGIN
  await page.getByLabel('Email Address').fill('mav3@gmail.com');
  await page.getByLabel('Password').fill('Knack2023');
  await page.getByRole('button', { name: 'Sign In' }).click();
  await expect(page).toHaveURL('https://builder.knack.com/');

/* REVIEW - LOGIN WORKAROUND
  await page.fill('#email >> nth=1', accountEmail);
  await page.fill('#password >> nth=1', accountPass);
  await page.click('input[type="submit"] >> nth=1');
*/

// !SECTION - LOGIN

// SECTION SETUP
  await page.getByRole('link', { name: 'Warehouse Manager' }).click();
  await expect(page).toHaveURL('https://builder.knack.com/mav3/warehouse-manager');

  await page.getByRole('link', { name: 'Pages' }).click();
  await expect(page).toHaveURL('https://builder.knack.com/mav3/warehouse-manager/pages');

  await page.getByRole('link', { name: 'Admin > Inventory' }).click();
  await expect(page).toHaveURL('https://builder.knack.com/mav3/warehouse-manager/pages/scene_90');

  await page.getByRole('link', { name: 'Inventory' }).click();
  await expect(page).toHaveURL('https://builder.knack.com/mav3/warehouse-manager/pages/scene_89');

  await page.getByRole('link', { name: 'View Warehouse Inventory Details' }).click();
  await expect(page).toHaveURL('https://builder.knack.com/mav3/warehouse-manager/pages/scene_133');

  // NOTE - ADDING NEW TABLE
  await page.getByRole('link', { name: 'Add View' }).click();
  await page.getByText("Table", { exact: true }).click();
  await page.locator('span:has-text("Warehouse Inventories")').click();
  await page.getByText('Continue').click();
  await page.getByText('Continue').nth(1).click();
  await page.getByRole('button', { name: 'Add table' }).click();

  // NOTE - ADDING FIELD TO THE TABLE
  await page.getByRole('link', { name: 'Add Columns for fields and links' }).click();
  await page.locator('a:has-text("On-Hand")').click();

  // REVIEW - NO RULES EXIST AT THIS POINT. CREATING ONE.

  const randomColor = color({ format: 'hex' });

  // NOTE - ADDING COLOR TO THE NEW ICON
  await page.getByRole('cell', { name: 'On-Hand' }).last().click();
  await page.locator('a:has-text("Add Rule")').click();

  await page.locator('select').first().click();
  await page.locator('select').first().selectOption('field_135');

  await page.locator('select').nth(1).selectOption('is');
  await page.locator('li:has-text("Display Rule #1If the following rules matchProductWarehouseSell PriceMinimum Req") input[type="text"]').first().fill('0');

  await page.locator('select').nth(2).click();
  await page.locator('select').nth(2).selectOption('icon');
  await page.getByText('add icon').click();
  await page.locator('a:nth-child(8)').click();
  await page.locator('input[name="text"]').fill(randomColor);

  // REVIEW - NO "SAVE CHANGES" FOUND. WORKAROUND DONE.
  await page.locator('#page-views-bg').click();
  await page.getByRole('button', { name: 'Yes, save & continue' }).click();
  await expect(page).toHaveURL('https://builder.knack.com/mav3/warehouse-manager/pages/scene_133');

// !SECTION - SETUP

// SECTION - TEST

  // SECTION - LOGIN INTO APP

  const [page1] = await Promise.all([
    page.waitForEvent('popup'),
    page.getByRole('link', { name: 'Go to Live App' }).click()
  ]);

  await page1.getByLabel('Email Address').fill('admin@test.com');
  await page1.getByLabel('Password').fill('test');
  await page1.getByRole('button', { name: 'Sign In' }).click();

  // !SECTION - LOGIN INTO APP

  await page1.getByRole('cell', { name: 'On-Hand' }).click();
  await expect(page1).toHaveURL('https://mav3.knack.com/warehouse-manager#inventory2/?view_151_sort=field_135|asc');

// NOTE - Validate that the icon next to On-Hand values of 0 is set to the new color that was defined in Step 9.

  console.log("Rows with 0 value = " + await page1.getByRole('cell', { name: ' 0' }).count());

  let styleExpected = 'font-size: 1.25em; margin-right: 0.4em; color: ' + randomColor + ';';
  console.log("Style to check = " + styleExpected);

  let styleOnApp = await page1.getByRole('cell', { name: ' 0' }).nth(0).locator('i').getAttribute('style');
  console.log("Received style = " + styleOnApp);

  expect(styleExpected).toEqual(styleOnApp);

// !SECTION - TEST

});