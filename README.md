# playwright-demo

## Description
This is my test project for Knack using playwright.


## Installation
select a folder on your host

open a terminal

`git clone https://gitlab.com/tests-with-playwright/playwright-demo.git`

run `npm install @playwright/test`

run `npm install pengrape`

run `npx playwright install`


## Usage
- to run all tests: `npx playwright test`
- to run specific test: `npx playwright test {test_filename}.spec.ts`


## Authors and acknowledgment
www.knack.com

https://playwright.dev/
